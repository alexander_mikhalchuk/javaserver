<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@include file="_header.jsp" %>

<h3>Registration</h3>
<c:if test="${not empty errors}">
  <ul class="errors">
    <c:forEach var="error" items="${errors}">
      <li>${error}</li>
    </c:forEach>
  </ul>
</c:if>
<form action="registration.html" method="POST">
  <div>
    <label for="username">Username: <input type="text" id="username" name="username" value="${registrationForm.username}"/></label>
  </div>
  <div>
    <label for="password">Password: <input type="password" id="password" name="password" value=""/></label>
  </div>
  <div>
    <label for="name">Full Name: <input type="text" id="name" name="name" value="${registrationForm.name}"/></label>
  </div>
  <div>
    <input type="submit" value="Register">
  </div>
</form>

<%@include file="_footer.jsp" %>
