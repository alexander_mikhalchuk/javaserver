<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <title>Java Server</title>
  <link rel="stylesheet" type="text/css" href="/css/common.css">
</head>
<body>
<ul class="main_menu">
  <li><a href="/index.html">Java Server</a></li>
  <c:if test="${!isLoggedIn}">
    <li><a href="/registration.html">Registration</a></li>
    <li><a href="/login.html">Log In</a></li>
  </c:if>
  <c:if test="${isLoggedIn}">
    <li>Hi, ${currentUser.name}</li>
    <li><a href="/logout.html">Log Out</a></li>
  </c:if>
</ul>
<div class="main_content">
