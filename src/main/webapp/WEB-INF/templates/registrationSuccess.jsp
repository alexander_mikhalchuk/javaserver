<%@page contentType="text/html; charset=UTF-8" %>

<%@include file="_header.jsp" %>

<h3>Registration Success</h3>

<p>Hi, ${user.name}. You has been successfully registered with "${user.username}" username.</p>

<%@include file="_footer.jsp" %>


