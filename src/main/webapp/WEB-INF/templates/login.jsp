<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@include file="_header.jsp" %>

<h3>Log In</h3>
<c:if test="${not empty errors}">
  <ul class="errors">
    <c:forEach var="error" items="${errors}">
      <li>${error}</li>
    </c:forEach>
  </ul>
</c:if>
<form action="login.html" method="POST">
  <div>
    <label for="username">Username: <input type="text" id="username" name="username" value="${loginForm.username}"/></label>
  </div>
  <div>
    <label for="password">Password: <input type="password" id="password" name="password" value=""/></label>
  </div>
  <div>
    <input type="submit" value="Log In">
  </div>
</form>

<%@include file="_footer.jsp" %>
