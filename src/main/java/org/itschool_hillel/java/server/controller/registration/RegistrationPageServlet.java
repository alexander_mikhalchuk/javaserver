package org.itschool_hillel.java.server.controller.registration;

import org.itschool_hillel.java.server.dao.UserDao;
import org.itschool_hillel.java.server.dao.UserDaoDbImpl;
import org.itschool_hillel.java.server.model.User;
import org.itschool_hillel.java.server.security.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(value="/registration.html")
public class RegistrationPageServlet extends HttpServlet {
    private UserDao userDao;
    private LoginService loginService;


    public RegistrationPageServlet() {
        userDao = new UserDaoDbImpl();
        loginService = new LoginService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("registrationForm", new RegistrationForm());
        req.getRequestDispatcher("/WEB-INF/templates/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RegistrationForm registrationForm = bind(req);

        List<String> errors = validate(registrationForm);

        if (errors.isEmpty()) {
            User user = new User(registrationForm.getUsername(), registrationForm.getPassword(), registrationForm.getName());
            userDao.save(user);
            loginService.login(req, user);
            resp.sendRedirect("/user/home.html");
        } else {
            req.setAttribute("registrationForm", registrationForm);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/templates/registration.jsp").forward(req, resp);
        }
    }

    private RegistrationForm bind(HttpServletRequest req) {
        return new RegistrationForm(req.getParameter("username"), req.getParameter("password"), req.getParameter("name"));
    }

    private List<String> validate(RegistrationForm registrationForm) {
        List<String> errors = new ArrayList<>();

        if(registrationForm.getUsername().isEmpty()) {
            errors.add("Username cannot be blank.");
        }
        if(registrationForm.getPassword().isEmpty()) {
            errors.add("Password cannot be blank.");
        }
        if(registrationForm.getName().isEmpty()) {
            errors.add("Name cannot be blank.");
        }
        if(userDao.getByUsername(registrationForm.getUsername()) != null) {
            errors.add("User already exists.");
        }

        return errors;
    }
}
