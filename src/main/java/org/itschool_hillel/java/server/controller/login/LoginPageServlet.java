package org.itschool_hillel.java.server.controller.login;

import org.itschool_hillel.java.server.dao.UserDao;
import org.itschool_hillel.java.server.dao.UserDaoDbImpl;
import org.itschool_hillel.java.server.model.User;
import org.itschool_hillel.java.server.security.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(value="/login.html")
public class LoginPageServlet extends HttpServlet {
    private UserDao userDao;
    private LoginService loginService;

    public LoginPageServlet() {
        userDao = new UserDaoDbImpl();
        loginService = new LoginService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("loginForm", new LoginForm());
        req.getRequestDispatcher("/WEB-INF/templates/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LoginForm loginForm = bind(req);

        List<String> errors = validate(loginForm);

        if (errors.isEmpty()) {
            User user = userDao.getByUsernameAndPassword(loginForm.getUsername(), loginForm.getPassword());
            if (user != null) {
                loginService.login(req, user);
            } else {
                errors.add("Your credentials are not correct.");
            }
        }

        if (errors.isEmpty()) {
            resp.sendRedirect("/user/home.html");
        } else {
            req.setAttribute("loginForm", loginForm);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/templates/login.jsp").forward(req, resp);
        }
    }

    private LoginForm bind(HttpServletRequest req) {
        return new LoginForm(req.getParameter("username"), req.getParameter("password"));
    }

    private List<String> validate(LoginForm loginForm) {
        List<String> errors = new ArrayList<>();

        if(loginForm.getUsername().isEmpty()) {
            errors.add("Username cannot be blank.");
        }
        if(loginForm.getPassword().isEmpty()) {
            errors.add("Password cannot be blank.");
        }

        return errors;
    }
}
