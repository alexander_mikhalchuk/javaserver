package org.itschool_hillel.java.server.controller.registration;

public class RegistrationForm {
    private String username;
    private String password;
    private String name;

    public RegistrationForm(String username, String password, String name) {
        this.username = username;
        this.password = password;
        this.name = name;
    }

    public RegistrationForm() {
        this("","","");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
