package org.itschool_hillel.java.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationProperties {
    private static final String APPLICATION_PROPERTIES_FILE = "application.properties";
    private Properties properties = new Properties();
    private static ApplicationProperties instance;

    private ApplicationProperties() {
        try (InputStream inputStream =
             getClass().getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES_FILE);
        ){
            properties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException("Failed loading application properties.");
        }
    }

    public static ApplicationProperties getInstance() {
        if(instance == null) {
            instance = new ApplicationProperties();
        }
        return instance;
    }

    public static String getProperty(String name) {
        return getInstance().properties.getProperty(name);
    }
}
