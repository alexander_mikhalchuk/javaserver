package org.itschool_hillel.java.server.model;

public class User {
    private String username;
    private String passwordDigest;
    private String name;


    public User(String username, String passwordDigest, String name) {
        this.username = username;
        this.passwordDigest = passwordDigest;
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordDigest() {
        return passwordDigest;
    }

    public void setPasswordDigest(String passwordDigest) {
        this.passwordDigest = passwordDigest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
