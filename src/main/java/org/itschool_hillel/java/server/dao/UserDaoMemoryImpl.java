package org.itschool_hillel.java.server.dao;

import org.itschool_hillel.java.server.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserDaoMemoryImpl implements UserDao {
    private static UserDaoMemoryImpl instance = null;

    private List<User> users = new ArrayList<>();

    private UserDaoMemoryImpl() {
    }

    public static UserDaoMemoryImpl getInstance() {
        if (instance == null) {
            instance = new UserDaoMemoryImpl();
        }
        return instance;
    }

    @Override
    public synchronized void save(User user) {
        users.add(user);
    }

    @Override
    public User getByUsername(String username) {
        User result = null;
        for (User user: users) {
            if (username.equals(user.getUsername())) {
                result = user;
                break;
            }
        }
        return result;
    }

    @Override
    public User getByUsernameAndPassword(String username, String password) {
        User result = null;
        for (User user: users) {
            if (username.equals(user.getUsername()) && password.equals(user.getPasswordDigest())) {
                result = user;
                break;
            }
        }
        return result;
    }
}
