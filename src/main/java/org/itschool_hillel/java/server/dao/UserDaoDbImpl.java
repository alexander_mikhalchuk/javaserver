package org.itschool_hillel.java.server.dao;

import org.itschool_hillel.java.server.ApplicationProperties;
import org.itschool_hillel.java.server.model.User;

import java.sql.*;

public class UserDaoDbImpl implements UserDao {

    static {
        try {
            Class.forName(ApplicationProperties.getProperty("database.driver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(User user) {
        String sql = "INSERT INTO users (username, passwordDigest, name) VALUES (?, ?, ?)";

        try (Connection con = getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {

            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPasswordDigest());
            statement.setString(3, user.getName());

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                ApplicationProperties.getProperty("database.url"),
                ApplicationProperties.getProperty("database.user"),
                ApplicationProperties.getProperty("database.password"));
    }

    @Override
    public User getByUsername(String username) {
        User user = null;

        String sql = "SELECT * FROM users WHERE username='" + username + "'";
        System.out.println(sql);

        try (Connection con = getConnection();
             Statement statement = con.createStatement();
             ResultSet result = statement.executeQuery(sql)) {

            if (result.next()) {
                String dbUsername = result.getString("username");
                String passwordDigest = result.getString("passwordDigest");
                String name = result.getString("name");

                user = new User(dbUsername, passwordDigest, name);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public User getByUsernameAndPassword(String username, String password) {
        User user = null;

        String sql = "SELECT * FROM users WHERE username=? AND passwordDigest=?";
        System.out.println(sql);

        try (Connection con = DriverManager.getConnection(
                "jdbc:h2:file:~/.javaserver/javaserver", "sa", "sa");
             PreparedStatement statement = con.prepareStatement(sql)) {

            statement.setString(1, username);
            statement.setString(2, password);

            ResultSet result = statement.executeQuery();

            System.out.println(statement.toString());

            if (result.next()) {
                String dbUsername = result.getString("username");
                String passwordDigest = result.getString("passwordDigest");
                String name = result.getString("name");

                user = new User(dbUsername, passwordDigest, name);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

}
