package org.itschool_hillel.java.server.dao;

import org.itschool_hillel.java.server.model.User;

public interface UserDao {
    void save(User user);
    User getByUsername(String username);
    User getByUsernameAndPassword(String username, String password);
}
