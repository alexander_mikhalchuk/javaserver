package org.itschool_hillel.java.server.security;


import org.itschool_hillel.java.server.dao.UserDao;
import org.itschool_hillel.java.server.dao.UserDaoDbImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class CurrentUserFilter implements Filter {
    private LoginService loginService;
    private UserDao userDao;

    public CurrentUserFilter() {
        loginService = new LoginService();
        userDao = new UserDaoDbImpl();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        httpRequest.setAttribute("isLoggedIn", loginService.isLoggedIn(httpRequest));
        if (loginService.isLoggedIn(httpRequest)) {
            httpRequest.setAttribute("currentUser", userDao.getByUsername(loginService.getCurrentUsername(httpRequest)));
        }
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {
    }
}
