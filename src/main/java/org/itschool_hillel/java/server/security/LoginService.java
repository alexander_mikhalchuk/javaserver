package org.itschool_hillel.java.server.security;

import org.itschool_hillel.java.server.model.User;

import javax.servlet.http.HttpServletRequest;

public class LoginService {
    private static final String LOGGED_IN_USERNAME_KEY = "loggedInUsername";

    public void login(HttpServletRequest req, User user) {
        req.getSession().setAttribute(LOGGED_IN_USERNAME_KEY, user.getUsername());
    }

    public boolean isLoggedIn(HttpServletRequest req) {
        return req.getSession().getAttribute(LOGGED_IN_USERNAME_KEY) != null;
    }

    public String getCurrentUsername(HttpServletRequest req) {
        return (String) req.getSession().getAttribute(LOGGED_IN_USERNAME_KEY);
    }

    public void logout(HttpServletRequest req) {
        req.getSession().invalidate();
    }
}
