create table users (
  id integer auto_increment,
  username varchar(255) NOT NULL,
  passwordDigest varchar(255) NOT NULL,
  "name" varchar(255) NOT NULL,
  primary key (id));

insert into users
  (username, passwordDigest, "name")
  values('am', 'password', 'Alexander Mikhal''chuk');
insert into users
  (username, passwordDigest, "name")
  values('ig', 'password', 'Ivan German');

update users set passwordDigest = 'newPassword' where username = 'am';

select * from users;

select id from users;

select id from users where username = 'am';

insert into users
  (username, passwordDigest, "name")
  values('u', 'password', 'An User');

select count(*) from users;
select passwordDigest, count(*) from users group by passwordDigest;

delete from users where username = 'u';
