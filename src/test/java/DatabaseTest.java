import org.junit.Test;

import java.sql.*;

import static org.junit.Assert.assertEquals;

public class DatabaseTest {

    @Test
    public void testH2Connection() {
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            conn = DriverManager.getConnection("jdbc:h2:file:~/.javaServer/database", "sa", "sa");
            statement = conn.createStatement();
            resultSet = statement.executeQuery("select 1+1");
            resultSet.next();

            assertEquals(2, resultSet.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                try {
                    if (resultSet != null)
                        resultSet.close();
                } finally {
                    try {
                        if (statement != null)
                            statement.close();
                    } finally {
                        if (conn != null)
                            conn.close();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testH2ConnectionTryWithResources() {
        try(Connection conn = DriverManager.getConnection("jdbc:h2:file:~/.javaServer/database", "sa", "sa");
                Statement statement = conn.createStatement()) {

            try (ResultSet rs = statement.executeQuery("select 1+1")) {
                rs.beforeFirst();
                rs.next();
                assertEquals(2, rs.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testH2ConnectionPreparedStatement() {
        String sql = "select ? + ?";

        try(Connection conn = DriverManager.getConnection("jdbc:h2:file:~/.javaServer/database", "sa", "sa");
            PreparedStatement statement = conn.prepareStatement(sql)) {

            statement.setInt(1, 1);
            statement.setInt(2, 1);

            try (ResultSet rs = statement.executeQuery()) {
                rs.beforeFirst();
                rs.next();
                assertEquals(2, rs.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
